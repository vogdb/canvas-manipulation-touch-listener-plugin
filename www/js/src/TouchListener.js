var previousInit = CanvasManipulation.prototype.init

CanvasManipulation.prototype.init = function () {
  previousInit.call(this, arguments)
  this.initTouchListeners()
}

CanvasManipulation.prototype.initTouchListeners = function () {
  var self = this
  var hammer = Hammer(this.getCanvas(), {'prevent_default': true, 'transform_min_rotation': 15})
  hammer.on('dragstart', function (evt) {
    self.dragStart(self.touchCoord(evt))
  })
  hammer.on('drag', function (evt) {
    if (self._isNextFrame(evt)) {
      self.drag(self.touchCoord(evt))
    }
  })
  hammer.on('dragend', function () {
    self.dragEnd()
  })
  hammer.on('pinchin', function (evt) {
    if (self._isNextFrame(evt)) {
      self.zoom(self.touchCoord(evt), -1)
    }
  })
  hammer.on('pinchout', function (evt) {
    if (self._isNextFrame(evt)) {
      self.zoom(self.touchCoord(evt), 1)
    }
  })
  hammer.on('rotate', function (evt) {
    if (self._isNextFrame(evt)) {
      self.rotateHandler(self.touchCoord(evt), evt.gesture.rotation * Math.PI / 180)
    }
  })
  hammer.on('release', function () {
    self.release()
  })
}

CanvasManipulation.prototype.touchCoord = function (evt) {
  var ex = evt.gesture.center.pageX
  var ey = evt.gesture.center.pageY
  return {x: ex, y: ey}
}

CanvasManipulation.prototype.rotateHandler = function (center, radians) {
  //is it a continue of the started rotation
  if (!isNaN(this._rotateStartAngle)) {
    this.rotate(center, radians - this._rotateStartAngle)
    this._rotateStartAngle = radians
  } else {
    this._isRotating = true
    this._rotateStartAngle = radians
    this.rotate(center, this._rotateStartAngle)
  }
}

CanvasManipulation.isAndroid = function () {
  return navigator.userAgent.toLowerCase().search('android') !== -1
}

if (CanvasManipulation.isAndroid()) {
  //Android can update canvas only this often
  CanvasManipulation.FRAME_FREQUENCY = 1000 / 4 // 1/4 of second in ms.
} else {
  //iOS can update canvas very often
  CanvasManipulation.FRAME_FREQUENCY = 1000 / 24 // 1/24 of second in ms.
}

CanvasManipulation.prototype._isNextFrame = function (evt) {
  var isFirstFrame = !this._lastFrameTimestamp
  var isFramePassed = evt.gesture.timeStamp - this._lastFrameTimestamp > CanvasManipulation.FRAME_FREQUENCY
  if (isFirstFrame || isFramePassed) {
    this._lastFrameTimestamp = evt.gesture.timeStamp
    return true
  }
  return false
}

CanvasManipulation.prototype.release = function () {
  this.dragEnd()
  this._isRotating = false
  this._rotateStartAngle = null
  this._lastFrameTimestamp = null
}