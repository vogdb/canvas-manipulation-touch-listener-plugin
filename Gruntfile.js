module.exports = function (grunt) {

  var compiler = require('superstartup-closure-compiler')

  grunt.initConfig({

    pkg: grunt.file.readJSON('package.json')

    ,banner: '/*!\n <%= pkg.title || pkg.name %> - v<%= pkg.version %> - '
      + '<%= grunt.template.today("yyyy-mm-dd") %>\n'
      + ' <%= (pkg.homepage ? "* " + pkg.homepage : pkg.repository.url) + "\\n" %>'
      + ' Copyright (c) <%= grunt.template.today("yyyy") %> <%= pkg.author %>;'
      + ' Licensed <%= _.pluck(pkg.licenses, "type").join(", ") %>\n*/\n'

    ,copy: {
      main: {
        files: [
          {expand: true, flatten: true, dest: 'www/js/dist', src: [
            'node_modules/jQuery/tmp/jquery.js'
            ,'node_modules/hammerjs/dist/hammer.min.js'
            ,'node_modules/hammerjs/plugins/hammer.fakemultitouch.js'
            ,'node_modules/hammerjs/plugins/hammer.showtouches.js'
            ,'node_modules/canvas-manipulation/dist/canvas-manipulation.min.js'
            ,'node_modules/canvas-manipulation/dist/canvas-manipulation-zoom-buttons.min.js'
          ]}
        ]
      }
    }

    ,closureCompiler: {
      options: {
        compilerFile: compiler.getPath()
        //,checkModified: true
        ,compilerOpts: {
          compilation_level: 'SIMPLE_OPTIMIZATIONS'
          ,warning_level:'QUIET'
          ,output_wrapper: '"(function(){%output%}).call(this);"'
        }
      }
      ,touch: {
        src: ['www/js/src/*.js']
        ,dest: ['www/js/dist/canvas-manipulation-touch-listener.min.js']
      }
    }

    ,watch: {
      files: ['Gruntfile.js', 'www/js/src/**/*']
      ,tasks: 'default'
    }

  })

  grunt.loadNpmTasks('grunt-contrib-watch')
  grunt.loadNpmTasks('grunt-contrib-copy')
  grunt.loadNpmTasks('grunt-closure-tools')

  grunt.registerTask('default', ['copy', 'closureCompiler'])

}
